# This is a simple approach to the famous Iris dataset (https://archive.ics.uci.edu/ml/datasets/Iris).
# Here I attempted to classify the flowers using both linear regression for Iris Setosa and nearest
# neighbour for Versicolor and Virginica, since the last two can't be linearly separated.

import math
import random

# values found by random walk
# f(sl, sw, pl, pw) = sl*x1 + sw*x2 + pl*x3 + pw*x4

def load_data(file):
    data = []
    with open(file, 'r') as f:
        rows = f.read().splitlines()
    
    for r in rows:
        data.append([ float(i) for i in r.split(',') ])

    return data

def test_weights(weights, data):
    error = 0

    for entry in data:
        f = weights[-1]

        for i in range(len(weights) - 1):
            f += weights[i] * entry[i]
        
        error += (entry[-1] - f) ** 2
    
    return error

def adjust_weights(weights):
    for i in range(len(weights)):
        r = 0
        while (r >= -epsilon and r <= epsilon):
            r = random.uniform(-maximum, maximum)
        weights[i] *= r

def classify(entry, weights):
    f = weights[-1]

    for i in range(len(weights) - 1):
        f += weights[i] * entry[i]
    
    if f < -1:
        f = -1
    elif f > 1:
        f = 1
    
    return round(f)

def nearest_neighbour(entry, data):
    best_index = 0
    best_value = 0

    for i in range(len(data)):
        dot = 0
        modx = 0
        mody = 0

        for j in range(len(entry) - 1):
            dot += entry[j] * data[i][j]
            modx += entry[j] ** 2
            mody += data[i][j] ** 2
        
        modx = math.sqrt(modx)
        mody = math.sqrt(mody)

        cosine = dot / (modx * mody)

        if cosine > best_value and cosine < 1: # I don't want to spoil the training set
            best_index = i
            best_value = cosine
    
    return int(data[best_index][-1])

# load the datasets
data = load_data(input("Training set: "))
test = load_data(input("Testing set: "))

# define best weight found in other runs
weights = [0.20754210219527303, -0.15469457506735665, 0.020198062507010686, 0.5543235098394634, -1.5818089985992114]
bestWeights = [i for i in weights]
bestError = test_weights(bestWeights, data)


calculateWeights = input("Calculate weights [y/n]? ")
if calculateWeights == 'y':
    i = int(input("Iterations to run: ")) # I used 10000 on most runs
    epsilon = float(input("Epsilon: ")) # I used 0.2 on the best run
    maximum = float(input("Maximum: ")) # I used 1.8 on the best run

    if i == 0:
        i = 10000

    while i > 0:
        err = 0
        try:
            err = test_weights(weights, data)
        except OverflowError as e:
            print("Something odd happened. Try runnnig again with some smaller values")
            break
        
        if err < bestError:
            bestError = err
            bestWeights = [i for i in weights ]
        
        adjust_weights(weights)
        

        i -= 1

    print("Best weights found: ", bestWeights)
    print("With error: ", bestError)
    
confusion = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

print("Testing classification")
for entry in test:
    i = int(entry[4] + 1)
    # try linear regression since it works for setosa
    j = classify(entry, bestWeights) + 1

    if j >= 0: # use nearest neighbour for the other 2 classes
        j = nearest_neighbour(entry, data) + 1

    confusion[i][j] += 1

print("Confusion Matrix")
for r in confusion:
    print(r[0], r[1], r[2])